(function ($) {

	var cache = {};
	var selectedValue = null;

	Drupal.autocomplete_id = Drupal.autocomplete_id || {};

	Drupal.behaviors.autocomplete_id = {

		attach: function(context, settings) {
			$('input.autocomplete-id-search', context).once('autocomplete', function () {

				var self  = $(this);
				var inner = $(this).parent();
				var outer = $(this).parent().parent();

				// Find the ID containing input
				var selected = $('input.autocomplete-id-selected', outer);

				var url = Drupal.settings.autocomplete_id[self.attr('id')];

				self.autocomplete({

					minLength: 1,

					create: function(event, ui) {
						self.after( $('<div>').attr('class', 'autocomplete-id-info') );
					},

					source: function (request, response) {
						var term = request.term;
						if (term in cache) {
							response(cache[term]);
							return;
						}
						$.getJSON(url + '/' + Drupal.encodePath(request.term), function(data, status, xhr) {
							cache[term] = data;
							response(data);
						});
					},

					select: function(event, ui) {
						// Store to compare against on input change
						selectedValue = ui.item.value;

						self.val(ui.item.value);
						selected.val(ui.item.id);

						console.log(ui.item);
					},

					change: function(event, ui) {
						if (self.val() != selectedValue) {
							selected.val('');
						}
					}
				}).data('autocomplete')._renderItem = function(ul, item) {
					li = $('<li>').data('item.autocomplete', item).append($('<a>').html(item.label));
					li.appendTo(ul);
				};
			});
		}

	}

})(jQuery);